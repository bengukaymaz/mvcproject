﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(webProjeOdevi.Startup))]
namespace webProjeOdevi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
